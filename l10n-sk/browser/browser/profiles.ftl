# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } – Výber používateľského profilu
profile-window-heading = Vyberte profil { -brand-short-name(case: "gen") }
profile-window-body = Udržujte svoje pracovné a osobné prehliadanie, vrátane vecí, ako sú heslá a záložky, úplne oddelené. Alebo vytvorte profily pre každého, kto používa toto zariadenie.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Zobrazovať vždy pri spustení { -brand-short-name(case: "gen") }
profile-window-create-profile = Vytvoriť profil
# Variables
#   $number (number) - The number of the profile
default-profile-name = Profil { $number }
edit-profile-page-title = Úprava profilu
edit-profile-page-header = Upravte svoj profil
edit-profile-page-profile-name-label = Názov profilu
edit-profile-page-theme-header = Téma vzhľadu
edit-profile-page-explore-themes = Preskúmajte ďalšie témy
edit-profile-page-avatar-header = Avatar
edit-profile-page-delete-button =
    .label = Odstrániť
edit-profile-page-no-name = Pomenujte tento profil, aby ste ho neskôr našli. Kedykoľvek ho môžete premenovať.
edit-profile-page-duplicate-name = Názov profilu sa už používa. Skúste iný názov.
edit-profile-page-profile-saved = Uložený
new-profile-page-title = Nový profil
new-profile-page-header = Prispôsobte si svoj nový profil
new-profile-page-header-description = Každý profil uchováva svoju jedinečnú históriu prehliadania a nastavenia oddelené od vašich ostatných profilov. Navyše, silná ochrana súkromia vo { -brand-short-name(case: "loc") } je predvolene zapnutá.
new-profile-page-learn-more = Ďalšie informácie
new-profile-page-input-placeholder =
    .placeholder = Zvoľte názov ako “Práca” alebo “Osobný”
new-profile-page-done-button =
    .label = Dokončiť úpravy

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Odstránenie profilu { $profilename }
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Odstrániť profil { $profilename }?
delete-profile-description = { -brand-short-name } natrvalo odstráni nasledujúce údaje z tohto zariadenia:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Otvorené okná
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Otvorené karty
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Záložky
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = História (navštívené stránky, súbory cookie, údaje stránok)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Automaticky vypĺňané údaje (adresy, spôsoby platby)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Heslá
# Button label
delete-profile-cancel = Zrušiť
# Button label
delete-profile-confirm = Odstrániť
