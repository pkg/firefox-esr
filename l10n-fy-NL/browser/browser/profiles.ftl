# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - Brûkersprofyl kieze
profile-window-heading = Kies in { -brand-short-name }-profyl
profile-window-body = Hâld jo wurk en persoanlike navigaasje, ynklusyf saken lykas wachtwurden en blêdwizers, folslein skieden. Of meitsje profilen foar elkenien dy’t dit apparaat brûkt.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Dit toane wannear’t { -brand-short-name } iepene wurdt
profile-window-create-profile = In profyl oanmeitsje
# Variables
#   $number (number) - The number of the profile
default-profile-name = Profyl { $number }
edit-profile-page-title = Profyl bewurkje
edit-profile-page-header = Jo profyl bewurkje
edit-profile-page-profile-name-label = Profylnamme
edit-profile-page-theme-header = Tema
edit-profile-page-explore-themes = Mear tema’s ferkenne
edit-profile-page-avatar-header = Avatar
edit-profile-page-delete-button =
    .label = Fuortsmite
edit-profile-page-no-name = Jou dit profyl in namme, sadat jo it letter fine kinne. Jo kinne de namme op elk winske momint wizigje.
edit-profile-page-duplicate-name = Profylnamme is al yn gebrûk. Probearje in nije namme.
edit-profile-page-profile-saved = Bewarre
new-profile-page-title = Nij profyl
new-profile-page-header = Jo nije profyl oanpasse
new-profile-page-header-description = Elk profyl hâldt syn unike navigaasjeskiednis en ynstellingen skieden fan jo oare profilen. Plus, de sterke privacybeskermingen fan { -brand-short-name } binne standert ynskeakele.
new-profile-page-learn-more = Mear ynfo
new-profile-page-input-placeholder =
    .placeholder = Kies in namme lykas ‘Wurk’ of ‘Persoanlik’
new-profile-page-done-button =
    .label = Bewurkjen foltôge

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Profyl { $profilename } fuortsmite
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Profyl { $profilename } fuortsmite?
delete-profile-description = { -brand-short-name } smyt de folgjende gegevens permanint fan dit apparaat fuort:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Iepen finsters
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Iepen ljeplêden
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Blêdwizers
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Skiednis (besochte siden, cookies, websitegegevens)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Gegevens automatysk ynfolje (adressen, betelmetoaden)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Wachtwurden
# Button label
delete-profile-cancel = Annulearje
# Button label
delete-profile-confirm = Fuortsmite
