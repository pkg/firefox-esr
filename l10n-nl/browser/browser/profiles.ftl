# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } – Gebruikersprofiel kiezen
profile-window-heading = Kies een { -brand-short-name }-profiel
profile-window-body = Houd uw werk- en persoonlijke navigatie, inclusief zaken als wachtwoorden en bladwijzers, volledig gescheiden. Of maak profielen aan voor iedereen die dit apparaat gebruikt.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Dit tonen wanneer { -brand-short-name } wordt geopend
profile-window-create-profile = Een profiel aanmaken
# Variables
#   $number (number) - The number of the profile
default-profile-name = Profiel { $number }
edit-profile-page-title = Profiel bewerken
edit-profile-page-header = Uw profiel bewerken
edit-profile-page-profile-name-label = Profielnaam
edit-profile-page-theme-header = Thema
edit-profile-page-explore-themes = Meer thema’s verkennen
edit-profile-page-avatar-header = Avatar
edit-profile-page-delete-button =
    .label = Verwijderen
edit-profile-page-no-name = Geef dit profiel een naam, zodat u het later kunt vinden. U kunt de naam op elk gewenst moment wijzigen.
edit-profile-page-duplicate-name = Profielnaam wordt al gebruikt. Probeer een nieuwe naam.
edit-profile-page-profile-saved = Opgeslagen
new-profile-page-title = Nieuw profiel
new-profile-page-header = Uw nieuwe profiel aanpassen
new-profile-page-header-description = Elk profiel houdt zijn unieke navigatiegeschiedenis en instellingen gescheiden van uw andere profielen. Bovendien zijn de sterke privacybeschermingen van { -brand-short-name } standaard ingeschakeld.
new-profile-page-learn-more = Meer info
new-profile-page-input-placeholder =
    .placeholder = Kies een naam zoals ‘Werk’ of ‘Persoonlijk’
new-profile-page-done-button =
    .label = Bewerken voltooid

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Profiel { $profilename } verwijderen
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Profiel { $profilename } verwijderen?
delete-profile-description = { -brand-short-name } verwijdert de volgende gegevens permanent van dit apparaat:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Open vensters
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Open tabbladen
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Bladwijzers
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Geschiedenis (bezochte pagina’s, cookies, websitegegevens)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Gegevens automatisch invullen (adressen, betalingsmethoden)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Wachtwoorden
# Button label
delete-profile-cancel = Annuleren
# Button label
delete-profile-confirm = Verwijderen
