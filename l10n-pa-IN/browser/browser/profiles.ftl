# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - ਵਰਤੋਂਕਾਰ ਪਰੋਫਾਈਲ ਚੁਣੋ
profile-window-heading = { -brand-short-name } ਪਰੋਫਾਈਲ ਚੁਣੋ
profile-window-body = ਆਪਣੇ ਕੰਮ ਅਤੇ ਨਿੱਜੀ ਬਰਾਊਜ਼ ਕਰਨ ਨੂੰ ਬਿਲਕੁਲ ਵੱਖੋ-ਵੱਖਰਾ ਰੱਖੋ, ਜਿਸ ਵਿੱਚ ਪਾਸਵਰਡ ਅਤੇ ਬੁੱਕਮਾਰਕ ਸ਼ਾਮਲ ਹਨ। ਜਾਂ ਇਸ ਡਿਵਾਈਸ ਵਰਤਣ ਵਾਲੇ ਹਰੇਕ ਵਾਪਸੇ ਵੱਖਰਾ ਪਰੋਫਾਈਲ ਬਣਾਓ।
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = ਜਦੋਂ { -brand-short-name } ਖੁੱਲ੍ਹਾ ਹੋਵੇ ਤਾਂ ਇਹ ਵੇਖਾਓ
profile-window-create-profile = ਪਰੋਫਾਈਲ ਬਣਾਓ
# Variables
#   $number (number) - The number of the profile
default-profile-name = ਪਰੋਫਾਈਲ { $number }
edit-profile-page-title = ਪਰੋਫਾਇਲ ਨੂੰ ਸੋਧੋ
edit-profile-page-header = ਆਪਣੇ ਪਰੋਫਾਈਲ ਵਿੱਚ ਸੋਧ ਕਰੋ
edit-profile-page-profile-name-label = ਪਰੋਫਾਈਲ ਦਾ ਨਾਂ
edit-profile-page-theme-header = ਥੀਮ
edit-profile-page-explore-themes = ਹੋਰ ਥੀਮ ਦੀ ਪੜਤਾਲ ਕਰੋ
edit-profile-page-avatar-header = ਅਵਤਾਰ
edit-profile-page-delete-button =
    .label = ਹਟਾਓ
edit-profile-page-no-name = ਇਸ ਪਰੋਫਾਈਲ ਦਾ ਨਾਂ, ਜੋ ਤੁਹਾਨੂੰ ਇਸ ਨੂੰ ਲੱਭਣ ਲਈ ਸਹਾਇਕ ਹੋਵੇ। ਇਸ ਦਾ ਨਾਂ ਕਿਸੇ ਵੇਲੇ ਵੀ ਬਦਲ ਸਕਦੇ ਹੋ।
edit-profile-page-duplicate-name = ਪਰੋਫਾਈਲ ਨਾਂ ਪਹਿਲਾਂ ਹੀ ਵਰਤਿਆ ਜਾ ਰਿਹਾ ਹੈ। ਵੱਖਰਾ ਨਾਂ ਅਜ਼ਮਾਓ।
edit-profile-page-profile-saved = ਸੰਭਾਲਿਆ
new-profile-page-title = ਨਵਾਂ ਪਰੋਫਾਈਲ
new-profile-page-header = ਆਪਣੇ ਨਵੇਂ ਪਰੋਫਾਈਲ ਨੂੰ ਕਸਟਮਾਈਜ਼ ਕਰੋ
new-profile-page-header-description = ਹਰ ਪਰੋਫਾਈਲ ਤੁਹਾਡੇ ਹੋਰ ਪਰੋਫਾਈਲਾਂ ਤੋਂ ਆਪਣੇ ਬਰਾਊਜ਼ ਕਰਨ ਦੇ ਅਤੀਤ ਅਤੇ ਸੈਟਿੰਗਾਂ ਨੂੰ ਵੱਖਰਾ ਰੱਖਦਾ ਹੈ। ਇਸ ਨਾਲ ਹੀ { -brand-short-name } ਦੀ ਮਜ਼ਬੂਤ ਪਰਦੇਦਾਰੀ ਸੁਰੱਖਿਆ ਵੀ ਮੂਲ ਰੂਪ ਵਿੱਚ ਚਾਲੂ ਹੁੰਦੀ ਹੈ।
new-profile-page-learn-more = ਹੋਰ ਜਾਣੋ
new-profile-page-input-placeholder =
    .placeholder = “ਕੰਮ” ਜਾਂ “ਨਿੱਜੀ” ਵਰਗਾ ਨਾਂ ਚੁਣੋ
new-profile-page-done-button =
    .label = ਸੋਧ ਕਰਨੀ ਪੂਰੀ ਹੋਈ

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = { $profilename } ਪਰੋਫਾਈਲ ਨੂੰ ਹਟਾਓ
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = { $profilename } ਪਰੋਫਾਈਲ ਨੂੰ ਹਟਾਉਣਾ ਹੈ?
delete-profile-description = { -brand-short-name } ਇਸ ਡਿਵਾਈਸ ਤੋਂ ਅੱਗੇ ਦਿੱਤੇ ਡਾਟੇ ਨੂੰ ਪੱਕੇ ਤੌਰ ਉੱਤੇ ਹਟਾ ਦੇਵੇਗਾ:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = ਵਿੰਡੋਆਂ ਨੂੰ ਖੋਲ੍ਹੋ
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = ਟੈਬਾਂ ਨੂੰ ਖੋਲ੍ਹੋ
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = ਬੁੱਕਮਾਰਕ
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = ਅਤੀਤ (ਖੋਲ੍ਹੇ ਗਏ ਸਫ਼ੇ, ਕੂਕੀਜ਼, ਸਾਈਟ ਡਾਟਾ)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = ਆਪਣੇ ਭਰਨ ਵਾਲਾ ਡਾਟਾ (ਸਿਰਨਾਵੇਂ, ਭੁਗਤਾਨ ਦੇ ਢੰਗ)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = ਪਾਸਵਰਡ
# Button label
delete-profile-cancel = ਰੱਦ ਕਰੋ
# Button label
delete-profile-confirm = ਹਟਾਓ
