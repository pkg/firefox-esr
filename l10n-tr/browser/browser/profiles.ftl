# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - Kullanıcı profilini seçin
profile-window-heading = Bir { -brand-short-name } profili seçin
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = { -brand-short-name } açıldığında bu pencereyi göster
profile-window-create-profile = Profil oluştur
# Variables
#   $number (number) - The number of the profile
default-profile-name = Profil { $number }
edit-profile-page-title = Profili düzenle
edit-profile-page-header = Profilinizi düzenleyin
edit-profile-page-profile-name-label = Profil adı
edit-profile-page-theme-header = Tema
edit-profile-page-explore-themes = Daha fazla tema keşfedin
edit-profile-page-avatar-header = Avatar
edit-profile-page-delete-button =
    .label = Sil
edit-profile-page-no-name = Daha sonra bulmanıza yardımcı olması için bu profile bir ad verin. Daha sonra adını değiştirebilirsiniz.
edit-profile-page-duplicate-name = Bu profil adı zaten kullanılıyor. Yeni bir ad deneyin.
edit-profile-page-profile-saved = Kaydedildi
new-profile-page-title = Yeni profil
new-profile-page-header = Yeni profilinizi özelleştirin
new-profile-page-learn-more = Daha fazla bilgi alın
new-profile-page-input-placeholder =
    .placeholder = “İş” veya “Kişisel” gibi bir ad seçin
new-profile-page-done-button =
    .label = Düzenlemeyi bitir

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = { $profilename } profilini sil
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = { $profilename } profili silinsin mi?
delete-profile-description = { -brand-short-name } bu cihazdan aşağıdaki verileri kalıcı olarak silecek:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Açık pencereler
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Açık sekmeler
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Yer imleri
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Geçmiş (ziyaret edilen sayfalar, çerezler, site verileri)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Otomatik doldurma verileri (adresler, ödeme yöntemleri)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Parolalar
# Button label
delete-profile-cancel = Vazgeç
# Button label
delete-profile-confirm = Sil
