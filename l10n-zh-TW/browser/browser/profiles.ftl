# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - 選取使用者設定檔
profile-window-heading = 選擇一份 { -brand-short-name } 設定檔
profile-window-body = 讓您工作與個人使用的上網紀錄（包含網站密碼、書籤等）完全拆開；也可以幫使用這台裝置的每個人建立自己的設定檔。
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = { -brand-short-name } 啟動時顯示本視窗
profile-window-create-profile = 建立設定檔
# Variables
#   $number (number) - The number of the profile
default-profile-name = 設定檔 { $number }
edit-profile-page-title = 編輯設定檔
edit-profile-page-header = 編輯您的設定檔
edit-profile-page-profile-name-label = 設定檔名稱
edit-profile-page-theme-header = 佈景主題
edit-profile-page-explore-themes = 探索更多佈景主題
edit-profile-page-avatar-header = 頭像
edit-profile-page-delete-button =
    .label = 刪除
edit-profile-page-no-name = 為此設定檔命名，方便之後尋找，可以隨時再更名。
edit-profile-page-duplicate-name = 已有這個名稱的設定檔，請改用其他名稱。
edit-profile-page-profile-saved = 已儲存
new-profile-page-title = 新增設定檔
new-profile-page-header = 自訂您的新設定檔
new-profile-page-header-description = 每一套設定檔之間的瀏覽紀錄、設定等均彼此獨立。另外，{ -brand-short-name } 強大的安全性保護設定都會預設開啟。
new-profile-page-learn-more = 更多資訊
new-profile-page-input-placeholder =
    .placeholder = 使用諸如「工作」或「個人」這樣的名稱
new-profile-page-done-button =
    .label = 編輯完成

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = 刪除 { $profilename } 設定檔
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = 要刪除 { $profilename } 設定檔嗎？
delete-profile-description = { -brand-short-name } 將從此裝置永久刪除下列資料：
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = 開啟視窗
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = 開啟分頁
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = 書籤
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = 瀏覽紀錄（造訪過的網頁、Cookie、網站資料等）
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = 表單自動填寫資料（地址、付款方式等）
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = 密碼
# Button label
delete-profile-cancel = 取消
# Button label
delete-profile-confirm = 刪除
