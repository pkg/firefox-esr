# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - Choisissez un profil utilisateur
profile-window-heading = Choisissez un profil { -brand-short-name }
profile-window-body = Séparez votre navigation professionnelle et personnelle, y compris par exemple vos mots de passe et vos marque-pages. Ou créez un profil pour toutes les personnes qui utilisent cet appareil.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Afficher à l’ouverture de { -brand-short-name }
profile-window-create-profile = Créer un profil
# Variables
#   $number (number) - The number of the profile
default-profile-name = Profil { $number }
edit-profile-page-title = Modifier le profil
edit-profile-page-header = Modifier votre profil
edit-profile-page-profile-name-label = Nom du profil
edit-profile-page-theme-header = Thème
edit-profile-page-explore-themes = Découvrir d’autres thèmes
edit-profile-page-avatar-header = Avatar
edit-profile-page-delete-button =
    .label = Supprimer

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

