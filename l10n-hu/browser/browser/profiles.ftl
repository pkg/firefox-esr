# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } – Felhasználói profil kiválasztása
profile-window-heading = Válasszon egy { -brand-short-name }-profilt
profile-window-body = Tartsa teljesen külön a munkahelyi és a személyes böngészését, beleértve a jelszavait és a könyvjelzőit. Vagy hozzon létre profilt mindenki számára, aki ezt az eszközt használja.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Megjelenítés a { -brand-short-name } megnyitásakor
profile-window-create-profile = Profil létrehozása
# Variables
#   $number (number) - The number of the profile
default-profile-name = { $number }. profil
edit-profile-page-title = Profil szerkesztése
edit-profile-page-header = Saját profil szerkesztése
edit-profile-page-profile-name-label = Profil neve
edit-profile-page-theme-header = Téma
edit-profile-page-explore-themes = További témák felfedezése
edit-profile-page-avatar-header = Profilkép
edit-profile-page-delete-button =
    .label = Törlés
edit-profile-page-no-name = Nevezze el ezt a profilt, hogy később segítsen megtalálni. Nevezze át bármikor.
edit-profile-page-duplicate-name = A profilnév már használatban van. Próbáljon meg egy új nevet.
edit-profile-page-profile-saved = Mentve
new-profile-page-title = Új profil
new-profile-page-header = Saját profil testreszabása
new-profile-page-header-description = Minden profil elkülöníti a saját egyedi böngészési előzményeit és beállításait a többi profiltól. Ráadásul a { -brand-short-name } erős adatvédelmi funkciói alapértelmezés szerint be vannak kapcsolva.
new-profile-page-learn-more = További tudnivalók
new-profile-page-input-placeholder =
    .placeholder = Válasszon egy nevet, például „Munkahelyi” vagy „Személyes”
new-profile-page-done-button =
    .label = Szerkesztés kész

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = A(z) { $profilename } profil törlése
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Törli a(z) { $profilename } profilt?
delete-profile-description = A { -brand-short-name } véglegesen törli a következő adatokat erről az eszközről:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Megnyitott ablakok
