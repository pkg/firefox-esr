# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } — выбор профиля пользователя
profile-window-heading = Выберите профиль { -brand-short-name }
profile-window-body = Полностью разделите работу и личный просмотр, включая пароли и закладки. Или создайте профили для всех, кто использует это устройство.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Показывать это при открытии { -brand-short-name }
profile-window-create-profile = Создать профиль
# Variables
#   $number (number) - The number of the profile
default-profile-name = Профиль { $number }
edit-profile-page-title = Редактировать профиль
edit-profile-page-header = Изменить ваш профиль
edit-profile-page-profile-name-label = Имя профиля
edit-profile-page-theme-header = Тема
edit-profile-page-explore-themes = Просмотреть больше тем
edit-profile-page-avatar-header = Аватар
edit-profile-page-delete-button =
    .label = Удалить
edit-profile-page-no-name = Дайте имя этому профилю, чтобы легче находить его позже. Изменяйте его имя в любое время.
edit-profile-page-duplicate-name = Имя профиля уже используется. Попробуйте новое имя.
edit-profile-page-profile-saved = Сохранено
new-profile-page-title = Новый профиль
new-profile-page-header = Настройка вашего нового профиля
new-profile-page-header-description = Каждый профиль хранит свою историю просмотров и настройки отдельно от других ваших профилей. Кроме того, надежная защита приватности { -brand-short-name } включена по умолчанию.
new-profile-page-learn-more = Подробнее
new-profile-page-input-placeholder =
    .placeholder = Выберите имя, например, «Работа» или «Личное».
new-profile-page-done-button =
    .label = Завершить редактирование

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Удалить профиль { $profilename }
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Удалить профиль { $profilename }?
delete-profile-description = { -brand-short-name } навсегда удалит следующие данные с этого устройства:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Открытые окна
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Открытые вкладки
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Закладки
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = История (посещённые страницы, куки, данные сайтов)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Данные автозаполнения (адреса, способы оплаты)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Пароли
# Button label
delete-profile-cancel = Отмена
# Button label
delete-profile-confirm = Удалить
