# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - Perfil de usuário
profile-window-heading = Escolha um perfil do { -brand-short-name }
profile-window-body = Mantenha sua navegação profissional e pessoal totalmente separadas, incluindo coisas como senhas e favoritos. Ou crie um perfil para cada pessoa que usa este dispositivo.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Mostrar ao abrir o { -brand-short-name }
profile-window-create-profile = Criar um perfil
# Variables
#   $number (number) - The number of the profile
default-profile-name = Perfil { $number }
edit-profile-page-title = Editar perfil
edit-profile-page-header = Editar seu perfil
edit-profile-page-profile-name-label = Nome do perfil
edit-profile-page-theme-header = Tema
edit-profile-page-explore-themes = Mais temas
edit-profile-page-avatar-header = Avatar
edit-profile-page-delete-button =
    .label = Excluir
edit-profile-page-no-name = Dê um nome a este perfil para ajudar a encontrar mais tarde. Renomeie quando quiser.
edit-profile-page-duplicate-name = Nome de perfil já em uso. Escolha outro nome.
edit-profile-page-profile-saved = Salvo
new-profile-page-title = Novo perfil
new-profile-page-header = Personalize seu novo perfil
new-profile-page-header-description = Cada perfil mantém seu próprio histórico de navegação e configurações, separado dos outros perfis. Além disso, as poderosas proteções de privacidade do { -brand-short-name } são ativadas por padrão.
new-profile-page-learn-more = Saiba mais
new-profile-page-input-placeholder =
    .placeholder = Escolha um nome como “Trabalho” ou “Pessoal”
new-profile-page-done-button =
    .label = Edição concluída

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Excluir perfil { $profilename }
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Excluir o perfil { $profilename }?
delete-profile-description = O { -brand-short-name } irá excluir permanentemente os seguintes dados deste dispositivo:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Janelas abertas
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Abas abertas
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Favoritos
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Histórico (páginas visitadas, cookies, dados de sites)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Dados de preenchimento automático (endereços, métodos de pagamento)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Senhas
# Button label
delete-profile-cancel = Cancelar
# Button label
delete-profile-confirm = Excluir
