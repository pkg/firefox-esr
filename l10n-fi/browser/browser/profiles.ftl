# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - Valitse käyttäjäprofiili
profile-window-heading = Valitse { -brand-short-name }-profiili
profile-window-body = Pidä työsi ja henkilökohtainen selaus, mukaan lukien salasanat ja kirjanmerkit, täysin erillään. Tai luo profiileja kaikille tämän laitteen käyttäjille.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Näytä tämä, kun { -brand-short-name } avautuu
profile-window-create-profile = Luo profiili
# Variables
#   $number (number) - The number of the profile
default-profile-name = Profiili { $number }
edit-profile-page-title = Muokkaa profiilia
edit-profile-page-header = Muokkaa profiiliasi
edit-profile-page-profile-name-label = Profiilin nimi
edit-profile-page-theme-header = Teema
edit-profile-page-explore-themes = Tutustu muihin teemoihin
edit-profile-page-avatar-header = Profiilikuva
edit-profile-page-delete-button =
    .label = Poista
edit-profile-page-no-name = Nimeä tämä profiili, jotta tunnistat sen myöhemmin. Nimeä se uudelleen milloin tahansa.
edit-profile-page-duplicate-name = Profiilin nimi on jo käytössä. Kokeile toista nimeä.
edit-profile-page-profile-saved = Tallennettu
new-profile-page-title = Uusi profiili
new-profile-page-header = Mukauta uutta profiiliasi
new-profile-page-header-description = Jokainen profiili pitää yksilöllisen selaushistorian ja asetukset erillään muista profiileistasi. Lisäksi { -brand-short-name }in vahvat yksityisyyden suojaukset ovat oletuksena käytössä.
new-profile-page-learn-more = Lue lisää
new-profile-page-input-placeholder =
    .placeholder = Valitse nimi, kuten "Työ" tai "Henkilökohtainen"
new-profile-page-done-button =
    .label = Muokkaus valmis

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Poista profiili { $profilename }
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Poistetaanko profiili { $profilename }?
delete-profile-description = { -brand-short-name } poistaa seuraavat tiedot pysyvästi tältä laitteelta:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Avoimet ikkunat
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Avoimet välilehdet
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Kirjanmerkit
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Historia (vieraillut sivut, evästeet, sivustotiedot)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Automaattisen täytön tiedot (osoitteet, maksutavat)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Salasanat
# Button label
delete-profile-cancel = Peruuta
# Button label
delete-profile-confirm = Poista
