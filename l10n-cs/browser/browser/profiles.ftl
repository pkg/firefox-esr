# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-create-profile = Vytvořit profil
# Variables
#   $number (number) - The number of the profile
default-profile-name = Profil { $number }
edit-profile-page-title = Upravit profil
edit-profile-page-header = Upravit profil
edit-profile-page-profile-name-label = Název profilu
edit-profile-page-theme-header = Motiv vzhledu
edit-profile-page-explore-themes = Prozkoumejte další motivy
edit-profile-page-avatar-header = Avatar
edit-profile-page-delete-button =
    .label = Smazat
edit-profile-page-profile-saved = Uloženo
new-profile-page-title = Nový profil
new-profile-page-learn-more = Zjistit více

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

