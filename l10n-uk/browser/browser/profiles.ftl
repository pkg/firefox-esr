# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } – вибір профілю користувача
profile-window-heading = Виберіть профіль { -brand-short-name }
profile-window-body = Відокремте робочий та особистий простір, зокрема паролі й закладки. Або створіть профілі для всіх, хто використовує цей пристрій.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Показувати це вікно під час відкриття { -brand-short-name }
profile-window-create-profile = Створити профіль
# Variables
#   $number (number) - The number of the profile
default-profile-name = Профіль { $number }
edit-profile-page-title = Редагувати профіль
edit-profile-page-header = Редагувати свій профіль
edit-profile-page-profile-name-label = Назва профілю
edit-profile-page-theme-header = Тема
edit-profile-page-explore-themes = Знайти інші теми
edit-profile-page-avatar-header = Аватар
edit-profile-page-delete-button =
    .label = Видалити
edit-profile-page-no-name = Назвіть цей профіль, щоб потім його було легше знайти. Змінити назву можна будь-коли.
edit-profile-page-duplicate-name = Назва профілю вже використовується. Спробуйте іншу.
edit-profile-page-profile-saved = Збережено
new-profile-page-title = Новий профіль
new-profile-page-header = Налаштуйте свій новий профіль
new-profile-page-header-description = Кожен профіль зберігає свою унікальну історію перегляду та налаштування окремо від інших ваших профілів. Крім того, надійний захист приватності { -brand-short-name } типово увімкнено.
new-profile-page-learn-more = Докладніше
new-profile-page-input-placeholder =
    .placeholder = Виберіть назву, наприклад, “Робота” або “Особистий”
new-profile-page-done-button =
    .label = Редагування виконано

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Видалити профіль { $profilename }
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Видалити профіль { $profilename }?
delete-profile-description = { -brand-short-name } остаточно видалить з цього пристрою такі дані:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Відкриті вікна
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Відкриті вкладки
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Закладки
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Історія (відвідані сторінки, файли cookie, дані сайтів)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Дані автозаповнення (адреси, способи оплати)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Паролі
# Button label
delete-profile-cancel = Скасувати
# Button label
delete-profile-confirm = Видалити
