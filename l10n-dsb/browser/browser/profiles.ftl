# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - Wužywaŕski profil wubraś
profile-window-heading = Wubjeŕśo profil { -brand-short-name }
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = To pokazaś, gaž se { -brand-short-name } wócynja
profile-window-create-profile = Profil załožyś
# Variables
#   $number (number) - The number of the profile
default-profile-name = Profil { $number }
edit-profile-page-title = Profil wobźěłaś
edit-profile-page-header = Wobźěłajśo swój profil
edit-profile-page-profile-name-label = Profilowe mě
edit-profile-page-theme-header = Drastwa
edit-profile-page-explore-themes = Wuslěźćo dalšne drastwy
edit-profile-page-avatar-header = Awatar
edit-profile-page-delete-button =
    .label = Lašowaś
edit-profile-page-no-name = Pómjeńśo toś ten profil, aby jen pózdźej zasej namakał. Pśemjeńśo jen kuždy cas.
edit-profile-page-duplicate-name = Profilowe mě se južo wužywa. Wopytajśo nowe mě.
edit-profile-page-profile-saved = Skłaźony
new-profile-page-title = Nowy profil
new-profile-page-header = Pśiměŕśo swój nowy profil
new-profile-page-learn-more = Dalšne informacije
new-profile-page-input-placeholder =
    .placeholder = Wubjeŕśo mě ako „Źěło“ abo „Wósobinske“
new-profile-page-done-button =
    .label = Wobźěłanje dokóńcone

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Profil { $profilename } lašowaś
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Profil { $profilename } lašowaś?
delete-profile-description = { -brand-short-name } na pśecej slědujuce daty z toś togo rěda wulašujo:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Wócynjone wokna
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Wócynjone rejtariki
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Cytańske znamjenja
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Historija (woglědane boki, cookieje, sedłowe daty)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Daty za awtomatiske wupołnjenje (adrese, płaśeńske metody)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Gronidła
# Button label
delete-profile-cancel = Pśetergnuś
# Button label
delete-profile-confirm = Lašowaś
