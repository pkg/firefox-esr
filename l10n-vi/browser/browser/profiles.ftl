# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - Chọn hồ sơ người dùng
profile-window-heading = Chọn một hồ sơ { -brand-short-name }
profile-window-body = Giữ công việc và duyệt web cá nhân của bạn, bao gồm những thứ như mật khẩu và dấu trang, hoàn toàn tách biệt. Hoặc tạo hồ sơ cho tất cả những người sử dụng thiết bị này.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Hiển thị điều này khi { -brand-short-name } mở
profile-window-create-profile = Tạo hồ sơ
# Variables
#   $number (number) - The number of the profile
default-profile-name = Hồ sơ { $number }
edit-profile-page-title = Sửa hồ sơ
edit-profile-page-header = Chỉnh sửa hồ sơ của bạn
edit-profile-page-profile-name-label = Tên hồ sơ
edit-profile-page-theme-header = Chủ đề
edit-profile-page-explore-themes = Khám phá chủ đề khác
edit-profile-page-avatar-header = Hình đại diện
edit-profile-page-delete-button =
    .label = Xóa
edit-profile-page-no-name = Đặt tên cho hồ sơ này để giúp bạn tìm thấy nó sau này. Có thể đổi tên bất cứ lúc nào.
edit-profile-page-duplicate-name = Tên hồ sơ đã được sử dụng. Hãy thử một tên khác.
edit-profile-page-profile-saved = Đã lưu
new-profile-page-title = Hồ sơ mới
new-profile-page-header = Tùy chỉnh hồ sơ mới của bạn
new-profile-page-header-description = Mỗi hồ sơ giữ lịch sử duyệt web và cài đặt riêng biệt với các hồ sơ khác của bạn. Thêm vào đó, chế độ bảo vệ quyền riêng tư mạnh mẽ của { -brand-short-name } vẫn được bật theo mặc định.
new-profile-page-learn-more = Tìm hiểu thêm
new-profile-page-input-placeholder =
    .placeholder = Chọn một tên như “Công việc” hoặc “Cá nhân”
new-profile-page-done-button =
    .label = Hoàn tất sửa

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Xoá hồ sơ { $profilename }
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Xoá hồ sơ { $profilename }?
delete-profile-description = { -brand-short-name } sẽ xóa vĩnh viễn dữ liệu sau khỏi thiết bị này:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Cửa sổ đang mở
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Thẻ đang mở
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Dấu trang
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Lịch sử (các trang đã truy cập, cookie, dữ liệu trang web)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Tự động điền dữ liệu (địa chỉ, phương thức thanh toán)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Mật khẩu
# Button label
delete-profile-cancel = Hủy bỏ
# Button label
delete-profile-confirm = Xóa
