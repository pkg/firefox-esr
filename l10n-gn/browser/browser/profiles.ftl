# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - Eiporavo poruhára mba’ete
profile-window-heading = Eiporavo poruhára mba’ete { -brand-short-name }
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Ehechauka kóva ijurujávo { -brand-short-name }
profile-window-create-profile = Emoheñói mba’ete
# Variables
#   $number (number) - The number of the profile
default-profile-name = Mba’ete { $number }
edit-profile-page-title = Embosako’i mba’ete
edit-profile-page-header = Embosako’i ne mba’ete
edit-profile-page-profile-name-label = Mba’ete réra
edit-profile-page-theme-header = Téma
edit-profile-page-explore-themes = Ehapykueho hetave téma
edit-profile-page-avatar-header = Avatar
edit-profile-page-delete-button =
    .label = Mboguete
edit-profile-page-duplicate-name = Pe mba’ete réra ojeporúma. Eheka téra pyahu.
edit-profile-page-profile-saved = Ñongatupyre
new-profile-page-title = Mba’ete pyahu
new-profile-page-header = Emboava pe mba’ete pyahu
new-profile-page-learn-more = Eikuaave
new-profile-page-input-placeholder =
    .placeholder = Eiporavo téra “Mba’apo” térã “Tapichaite”
new-profile-page-done-button =
    .label = Emohu’ã mbasako’i

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Embogue mba’ete { $profilename }
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = Embogue mba’ete { $profilename }
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Ovetã ijurujáva
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Tendayke ijurujáva
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Techaukahakuéra
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Tembiasakue (kuatiarogue eikehague, kookie, tenda mba’ekuaarã)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Mba’ekuaarã myanyhẽrã (kundaharape, ñehepyme’ẽrã)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Ñe’ẽñemikuéra
# Button label
delete-profile-cancel = Heja
# Button label
delete-profile-confirm = Mboguete
