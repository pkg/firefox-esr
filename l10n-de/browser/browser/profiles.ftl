# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

edit-profile-page-title = Profil bearbeiten
edit-profile-page-profile-name-label = Profilname
edit-profile-page-theme-header = Theme
edit-profile-page-avatar-header = Profilbild
edit-profile-page-delete-button =
    .label = Löschen
new-profile-page-learn-more = Weitere Informationen

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Offene Fenster
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Offene Tabs
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Lesezeichen
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Chronik (besuchte Seiten, Cookies, Website-Daten)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Passwörter
# Button label
delete-profile-cancel = Abbrechen
# Button label
delete-profile-confirm = Löschen
