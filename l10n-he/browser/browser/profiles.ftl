# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = ‏{ -brand-short-name } - בחירת פרופיל משתמש
profile-window-heading = בחירת פרופיל { -brand-short-name }
profile-window-body = ניתן לבחור לשמור את העבודה והגלישה האישית שלך, כולל דברים כמו ססמאות או סימניות, מופרדים לחלוטין. או ליצור פרופילים עבור כל מי שמשתמש במכשיר זה.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = להציג הודעה זו כאשר { -brand-short-name } נפתח
profile-window-create-profile = יצירת פרופיל
# Variables
#   $number (number) - The number of the profile
default-profile-name = פרופיל { $number }
edit-profile-page-title = עריכת פרופיל
edit-profile-page-header = עריכת הפרופיל שלך
edit-profile-page-profile-name-label = שם פרופיל
edit-profile-page-theme-header = ערכת נושא
edit-profile-page-explore-themes = עיון בערכות נושא נוספות
edit-profile-page-avatar-header = דמות מייצגת
edit-profile-page-delete-button =
    .label = מחיקה
edit-profile-page-no-name = ניתן לתת שם לפרופיל זה כדי לסייע לך למצוא אותו מאוחר יותר. אפשר לשנות את השם שלו בכל עת.
edit-profile-page-duplicate-name = שם הפרופיל כבר בשימוש. נא לנסות שם חדש.
edit-profile-page-profile-saved = נשמר
new-profile-page-title = פרופיל חדש
new-profile-page-header = התאמה אישית של הפרופיל החדש שלך
new-profile-page-header-description = כל פרופיל שומר על היסטוריית הגלישה וההגדרות הייחודיות שלו בנפרד מהפרופילים האחרים שלך. בנוסף, הגנות הפרטיות החזקות של { -brand-short-name } פועלות כברירת מחדל.
new-profile-page-learn-more = מידע נוסף
new-profile-page-input-placeholder =
    .placeholder = אפשר לבחור שם כמו ״עבודה״ או ״אישי״
new-profile-page-done-button =
    .label = סיום עריכה

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = מחיקת הפרופיל { $profilename }
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = למחוק את הפרופיל { $profilename }?
delete-profile-description = ‏{ -brand-short-name } ימחק לצמיתות את הנתונים הבאים ממכשיר זה:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = חלונות פתוחים
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = לשוניות פתוחות
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = סימניות
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = היסטוריה (דפים שביקרת בהם, עוגיות, נתוני אתרים)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = נתוני מילוי אוטומטי (כתובות, אמצעי תשלום)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = ססמאות
# Button label
delete-profile-cancel = ביטול
# Button label
delete-profile-confirm = מחיקה
