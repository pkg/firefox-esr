# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - Selecciona un perfil de usuario
profile-window-heading = Elige un perfil de { -brand-short-name }
profile-window-body = Mantén tu navegación laboral y personal, incluidas las contraseñas y los marcadores, totalmente separadas. O bien, crea perfiles para todos los usuarios de este dispositivo.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = Mostrar esto cuando se abra { -brand-short-name }
profile-window-create-profile = Crear un perfil
# Variables
#   $number (number) - The number of the profile
default-profile-name = Perfil { $number }
edit-profile-page-title = Editar perfil
edit-profile-page-header = Edita tu perfil
edit-profile-page-profile-name-label = Nombre del perfil
edit-profile-page-theme-header = Tema
edit-profile-page-explore-themes = Explorar más temas
edit-profile-page-avatar-header = Avatar
edit-profile-page-delete-button =
    .label = Eliminar
edit-profile-page-no-name = Ponle un nombre a este perfil para que te resulte fácil encontrarlo más tarde. Cámbiale el nombre cuando quieras.
edit-profile-page-duplicate-name = El nombre del perfil ya está en uso. Prueba con un nombre nuevo.
edit-profile-page-profile-saved = Guardado
new-profile-page-title = Nuevo perfil
new-profile-page-header = Personaliza tu nuevo perfil
new-profile-page-header-description = Cada perfil mantiene su historial de navegación y sus configuraciones únicos separados de los demás perfiles. Además, las sólidas protecciones de privacidad de { -brand-short-name } están activadas de forma predeterminada.
new-profile-page-learn-more = Aprender más
new-profile-page-input-placeholder =
    .placeholder = Elige un nombre como “Trabajo” o “Personal”
new-profile-page-done-button =
    .label = Edición terminada

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = Eliminar el perfil { $profilename }
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = ¿Eliminar el perfil { $profilename }?
delete-profile-description = { -brand-short-name } eliminará permanentemente los siguientes datos de este dispositivo:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Abrir ventanas
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Pestañas abiertas
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Marcadores
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Historial (páginas visitadas, cookies, datos del sitio)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Autocompletar datos (direcciones, métodos de pago)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Contraseñas
# Button label
delete-profile-cancel = Cancelar
# Button label
delete-profile-confirm = Eliminar
