# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

profile-window-title = { -brand-short-name } - пайдаланушы профилін таңдау
profile-window-heading = { -brand-short-name } профилін таңдау
profile-window-body = Парольдер мен бетбелгілер сияқты нәрселерді қоса алғанда, жұмыс пен жеке шолуды бөлек ұстаңыз. Немесе осы құрылғыны пайдаланатын барлық адамдар үшін профильдер жасаңыз.
# This checkbox appears in the choose profile window that appears when the browser is opened. "Show this" refers to choose profile window being shown when the checkbox is enabled.
profile-window-checkbox-label =
    .label = { -brand-short-name } ашылған кезде мұны көрсету
profile-window-create-profile = Профиль жасау
# Variables
#   $number (number) - The number of the profile
default-profile-name = Профиль { $number }
edit-profile-page-title = Профильді түзету
edit-profile-page-header = Профиліңізді түзету
edit-profile-page-profile-name-label = Профиль атауы
edit-profile-page-theme-header = Тема
edit-profile-page-explore-themes = Көбірек темаларды шолу
edit-profile-page-avatar-header = Аватар
edit-profile-page-delete-button =
    .label = Өшіру
edit-profile-page-no-name = Бұл профильді кейін оңай табу үшін атаңыз. Кез келген уақытта атын өзгертуге болады.
edit-profile-page-duplicate-name = Профиль атауы қолдануда болып тұр. Жаңа атауды қолданып көріңіз.
edit-profile-page-profile-saved = Сақталды
new-profile-page-title = Жаңа профиль
new-profile-page-header = Жаңа профиліңізді баптаңыз
new-profile-page-header-description = Әрбір профиль өзінің бірегей шолу журналы мен параметрлерін басқа профильдерден бөлек сақтайды. Оған қоса, { -brand-short-name } жекелікті мықты қорғауы іске қосылған.
new-profile-page-learn-more = Көбірек білу
new-profile-page-input-placeholder =
    .placeholder = «Жұмыс» немесе «Жеке» сияқты атауды таңдаңыз.
new-profile-page-done-button =
    .label = Түзету аяқталды

## Delete profile page that allows users to review what they will lose if they choose to delete their profile.

# Variables
#   $profilename (String) - The name of the profile.
delete-profile-page-title = { $profilename } профилін өшіру
# Variables
#   $profilename (String) - The name of the profile.
delete-profile-header = { $profilename } профилін өшіру керек пе?
delete-profile-description = { -brand-short-name } осы құрылғыдан келесі деректерді біржола өшіреді:
# Opened browser windows saved to a profile. This is followed by a column with the number of open windows associated to the profile.
delete-profile-windows = Ашық терезелер
# Opened browser tabs saved to a profile. This is followed by a column with the number of open tabs associated to the profile.
delete-profile-tabs = Ашық беттер
# Bookmarks saved to a profile. This is followed by a column with the number of bookmarks associated to the profile.
delete-profile-bookmarks = Бетбелгілер
# History saved to a profile. This is followed by a column with the number of visited pages / cookies / site data associated to the profile.
delete-profile-history = Тарих (ашылған беттер, cookie файлдары, сайт деректері)
# Autofill data saved to a profile. This is followed by a column with the number of addresses / payment methods associated to the profile.
delete-profile-autofill = Автотолтыру деректері (адрестер, төлем әдістері)
# Passwords saved to a profile. This is followed by a column with the number of saved passwords associated to the profile.
delete-profile-logins = Парольдер
# Button label
delete-profile-cancel = Бас тарту
# Button label
delete-profile-confirm = Өшіру
